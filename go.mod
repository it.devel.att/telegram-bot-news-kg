module gitlab.com/it.devel.att/telegram-bot-news-kg

go 1.13

require (
	github.com/PuerkitoBio/goquery v1.5.1
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/joho/godotenv v1.3.0
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
)
