package main

import (
	"log"
	"os"
	"strconv"
	"strings"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/joho/godotenv"
	"gitlab.com/it.devel.att/telegram-bot-news-kg/parsers"
)

var TELEGRAM_GROUP_ID int64
var TELEGRAM_BOT_TOKEN string

func init() {
	env := os.Getenv("NEWS_KG_ENV")

	switch env {
	case "":
		err := godotenv.Load(".env.development")
		if err != nil {
			log.Fatal("Error loading env.development file")
		}
	case "test":
		err := godotenv.Load(".env.test")
		if err != nil {
			log.Fatal("Error loading env.test file")
		}
	case "production":
		err := godotenv.Load(".env.production")
		if err != nil {
			log.Fatal("Error loading env.production file")
		}
	}
}

func main() {
	TELEGRAM_BOT_TOKEN = os.Getenv("TELEGRAM_BOT_TOKEN")
	TELEGRAM_GROUP_ID, err := strconv.ParseInt(os.Getenv("TELEGRAM_GROUP_ID"), 10, 64)

	bot, err := tgbotapi.NewBotAPI(TELEGRAM_BOT_TOKEN)
	if err != nil {
		log.Panic(err)
	}
	posts := make(chan parsers.Post)

	go parsers.GetLastPost24kg(posts)
	go parsers.GetLastPostKaktusKg(posts)
	go parsers.GetLastPostAkipressOrg(posts)
	go parsers.GetLastPostSputnikKg(posts)

	log.Println("Start parsing news and broadcast them into telegram group")

	for post := range posts {
		log.Printf("Send new post \"%v\" from source %v to telegram group", strings.TrimSpace(post.Title), post.Source)

		if len(post.FullMessage()) < 4096 {
			msg := tgbotapi.NewMessage(TELEGRAM_GROUP_ID, post.FullMessage())
			_, err := bot.Send(msg)
			if err != nil {
				log.Printf("Fails to send message to telegram group %v", err)
			}
		} else {
			msg := tgbotapi.NewMessage(TELEGRAM_GROUP_ID, post.ShortMessage())
			_, err := bot.Send(msg)
			if err != nil {
				log.Printf("Fails to send message to telegram group %v", err)
			}
		}
	}
}
