package parsers

import (
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
)

var SOURCE_SPUTNIK_KG string = "ru.sputnik.kg"
var SITE_SPUTKIN_KG string = "https://ru.sputnik.kg/"

func GetLastPostSputnikKg(posts chan Post) {
	for {
		var post Post
		time.Sleep(4 * time.Second)
		resSputkinKG, err := http.Get(NEWS_SITE_SPUTKIN_KG)
		if err != nil {
			log.Println(err)
			return
		}
		defer resSputkinKG.Body.Close()
		if resSputkinKG.StatusCode != 200 {
			log.Printf("status code error: %d %s", resSputkinKG.StatusCode, resSputkinKG.Status)
			return
		}

		resSputkinKgHTMLdoc, err := goquery.NewDocumentFromReader(resSputkinKG.Body)
		if err != nil {
			log.Println(err)
			return
		}

		lastPost := resSputkinKgHTMLdoc.Find("li.b-plainlist__item").First()
		lastPostDatetime := lastPost.Find("span.b-plainlist__date").Text()
		lastPostTitle := lastPost.Find("h2.b-plainlist__title").Text()
		lastPostTime := strings.Split(lastPostDatetime, " ")[0]
		if lastPostTitle != LAST_NEWS_SPUTNIK_KG_TITLE {
			LAST_NEWS_SPUTNIK_KG_TITLE = lastPostTitle
			post.Source = SOURCE_SPUTNIK_KG
			post.Title = lastPostTime + " " + lastPostTitle

			fullPostPageLink, _ := lastPost.Find("h2.b-plainlist__title").Find("a").Attr("href")
			post.Link = SITE_SPUTKIN_KG + fullPostPageLink

			resFullPostHTMLpage, err := http.Get(SITE_SPUTKIN_KG + fullPostPageLink)
			if err != nil {
				log.Println(err)
				return
			}
			defer resFullPostHTMLpage.Body.Close()
			if resFullPostHTMLpage.StatusCode != 200 {
				log.Printf("status code error: %d %s", resFullPostHTMLpage.StatusCode, resFullPostHTMLpage.Status)
				return
			}

			fullNewsPageLinkDocument, err := goquery.NewDocumentFromReader(resFullPostHTMLpage.Body)
			if err != nil {
				log.Println(err)
				return
			}

			descriptions := []string{}
			descriptionBlock := fullNewsPageLinkDocument.Find(".b-article__text").First()

			descriptionBlock.Find("p").Each(func(i int, postSelector *goquery.Selection) {
				descriptions = append(descriptions, postSelector.Text())
			})

			post.Description = strings.Join(descriptions, "\n")
			posts <- post
		}
	}
}
