package parsers

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
)

var SOURCE_KAKTUS_KG = "kaktus.media"

func GetLastPostKaktusKg(posts chan Post) {
	for {
		var post Post
		currentDate := time.Now()
		time.Sleep(3 * time.Second)
		formattedDate := fmt.Sprintf("%d-%02d-%02d", currentDate.Year(), currentDate.Month(), currentDate.Day())

		kaktusMediaNews, err := http.Get(fmt.Sprintf(NEWS_SITE_KAKTUS_KG, formattedDate))
		if err != nil {
			log.Println(err)
			return
		}
		defer kaktusMediaNews.Body.Close()
		if kaktusMediaNews.StatusCode != 200 {
			log.Printf("status code error: %d %s", kaktusMediaNews.StatusCode, kaktusMediaNews.Status)
			return
		}

		kaktusMediaNewsHTMLdoc, err := goquery.NewDocumentFromReader(kaktusMediaNews.Body)
		if err != nil {
			log.Println(err)
			return
		}

		lastNewsList := kaktusMediaNewsHTMLdoc.Find("ul .topic_list.view_lenta").First()
		lastPost := lastNewsList.Find("li .topic_item.clearfix.topic_actual").Last()
		postTime := lastPost.Find("span.topic_time_create.topic_actual").Text()

		postTitle := lastPost.Find("span.n").Text()
		if postTitle != LAST_NEWS_KAKTUS_KG_TITLE {
			LAST_NEWS_KAKTUS_KG_TITLE = postTitle

			post.Source = SOURCE_KAKTUS_KG
			post.Title = postTime + " " + postTitle + "\n"

			fullPostKaktusPageLink, postKaktusPageLinkExists := lastPost.Find("a").Attr("href")
			if postKaktusPageLinkExists {
				post.Link = fullPostKaktusPageLink

				fullPostKaktusPage, err := http.Get(fullPostKaktusPageLink)
				if err != nil {
					log.Println(err)
					return
				}
				defer fullPostKaktusPage.Body.Close()
				if fullPostKaktusPage.StatusCode != 200 {
					log.Printf("status code error: %d %s", fullPostKaktusPage.StatusCode, fullPostKaktusPage.Status)
					return
				}

				fullNewsPageLinkDocument, err := goquery.NewDocumentFromReader(fullPostKaktusPage.Body)
				if err != nil {
					log.Println(err)
					return
				}

				descriptions := []string{}
				descriptionBlock := fullNewsPageLinkDocument.Find("div.topic-text").First()

				descriptionBlock.Find("p").Each(func(i int, postSelector *goquery.Selection) {
					descriptions = append(descriptions, postSelector.Text())
				})

				post.Description = strings.Join(descriptions, "\n")
				posts <- post
			} else {
				log.Println("Link for fullPostKaktusPageLink didn't exist")
				posts <- post
			}
		}
	}
}
