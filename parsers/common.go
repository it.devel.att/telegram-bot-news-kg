package parsers

var LAST_NEWS_24_KG_TITLE string
var NEWS_SITE_24_KG string = "https://24.kg"
var LAST_NEWS_KAKTUS_KG_TITLE string
var NEWS_SITE_KAKTUS_KG string = "https://kaktus.media/?date=%v&lable=8&order=main"
var LAST_NEWS_AKIPRESS_ORG_TITLE string
var NEWS_SITE_AKIPRESS_ORG string = "https://akipress.org/"
var LAST_NEWS_SPUTNIK_KG_TITLE string
var NEWS_SITE_SPUTKIN_KG string = "https://ru.sputnik.kg/news/"

type Post struct {
	Source      string
	Title       string
	Description string
	Link        string
}

func (post Post) FullMessage() string {
	if post.Description == "" {
		return post.ShortMessage()
	}
	return post.Source + "\n" + post.Title + "\n" + post.Description
}

func (post Post) ShortMessage() string {
	return post.Source + "\n" + post.Title + "\n" + post.Link
}
