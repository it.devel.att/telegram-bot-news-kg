package parsers

import (
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
)

var SOURCE_24_KG string = "24.kg"

func GetLastPost24kg(posts chan Post) {
	for {
		var post Post
		time.Sleep(2 * time.Second)
		res24KG, err := http.Get(NEWS_SITE_24_KG)
		if err != nil {
			log.Println(err)
			return
		}
		defer res24KG.Body.Close()
		if res24KG.StatusCode != 200 {
			log.Printf("status code error: %d %s", res24KG.StatusCode, res24KG.Status)
			return
		}

		res24kgHTMLdoc, err := goquery.NewDocumentFromReader(res24KG.Body)
		if err != nil {
			log.Println(err)
			return
		}

		lastPost := res24kgHTMLdoc.Find("#newslist .one").First()
		lastPostTime := lastPost.Find(".time").Text()
		lastPostTitle := lastPost.Find(".title").Text()
		if lastPostTitle != LAST_NEWS_24_KG_TITLE {
			LAST_NEWS_24_KG_TITLE = lastPostTitle
			post.Source = SOURCE_24_KG
			post.Title = lastPostTime + lastPostTitle

			fullPostPageLink, _ := lastPost.Find("a").Attr("href")
			post.Link = NEWS_SITE_24_KG + fullPostPageLink

			res, err := http.Get(NEWS_SITE_24_KG + fullPostPageLink)
			if err != nil {
				log.Println(err)
				return
			}
			defer res.Body.Close()
			if res.StatusCode != 200 {
				log.Printf("status code error: %d %s", res.StatusCode, res.Status)
				return
			}

			fullNewsPageLinkDocument, err := goquery.NewDocumentFromReader(res.Body)
			if err != nil {
				log.Println(err)
				return
			}

			descriptions := []string{}
			descriptionBlock := fullNewsPageLinkDocument.Find(".cont").First()

			descriptionBlock.Find("p").Each(func(i int, postSelector *goquery.Selection) {
				descriptions = append(descriptions, postSelector.Text())
			})

			post.Description = strings.Join(descriptions, "\n")
			posts <- post
		}
	}
}
