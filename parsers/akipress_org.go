package parsers

import (
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
)

var SOURCE_AKIPRESS_ORG = "akipress.org"

func GetLastPostAkipressOrg(posts chan Post) {
	for {
		var post Post
		time.Sleep(2 * time.Second)

		akipressNews, err := http.Get(NEWS_SITE_AKIPRESS_ORG)
		if err != nil {
			log.Println(err)
			return
		}
		defer akipressNews.Body.Close()

		if akipressNews.StatusCode != 200 {
			log.Printf("status code error: %d %s", akipressNews.StatusCode, akipressNews.Status)
			return
		}

		akipressNewsHTMLdoc, err := goquery.NewDocumentFromReader(akipressNews.Body)
		if err != nil {
			log.Println(err)
			return
		}

		lastNewsList := akipressNewsHTMLdoc.Find("table.sections.section-last").First()
		lastPost := lastNewsList.Find("tr").First()
		postTime := lastPost.Find("td.datetxt").Text()

		postTitle := lastPost.Find("a.newslink").Text()
		if postTitle != LAST_NEWS_AKIPRESS_ORG_TITLE {
			LAST_NEWS_AKIPRESS_ORG_TITLE = postTitle

			post.Source = SOURCE_AKIPRESS_ORG
			post.Title = strings.TrimSpace(postTime) + postTitle + "\n"

			fullPostAkipressPageLink, AkipressPageLinkExists := lastPost.Find("a").Attr("href")
			if AkipressPageLinkExists {
				post.Link = fullPostAkipressPageLink

				fullPostAkipressPage, err := http.Get(fullPostAkipressPageLink)

				if err != nil {
					log.Println(err)
					return
				}
				defer fullPostAkipressPage.Body.Close()
				if fullPostAkipressPage.StatusCode != 200 {
					log.Printf("status code error: %d %s", fullPostAkipressPage.StatusCode, fullPostAkipressPage.Status)
					return
				}

				fullNewsPageDocument, err := goquery.NewDocumentFromReader(fullPostAkipressPage.Body)
				if err != nil {
					log.Println(err)
					return
				}

				descriptions := []string{}
				descriptionBlock := fullNewsPageDocument.Find("#newstext")

				if descriptionBlock.Text() == "" {
					descriptionBlock = fullNewsPageDocument.Find("#news_text")
				}

				descriptionBlock.Find("p").Each(func(i int, postSelector *goquery.Selection) {
					descriptions = append(descriptions, postSelector.Text())
				})

				post.Description = strings.Join(descriptions, "\n")
				posts <- post
			} else {
				log.Println("Link for fullPostAkipressPageLink didn't exist")
				posts <- post
			}
		}
	}
}
